package utils;

import com.fasterxml.jackson.databind.node.ObjectNode;
import play.libs.Json;
import play.mvc.Result;

import static play.mvc.Results.ok;

/**
 * Created by ducdang.pham on 2/3/2017.
 */

public class SimpleResult {

    public static Result success(String msg){
        ObjectNode responseObj = Json.newObject();
        responseObj.put("code", 200);
        responseObj.put("message", msg);
        return ok(responseObj);
    }

    public static Result success(){
        ObjectNode responseObj = Json.newObject();
        responseObj.put("code", 200);
        responseObj.put("message", "Success");
        return ok(responseObj);
    }

    public static Result systemError() {
        return error("errorRetryLater");
    }

    public static Result error(String msg){
        ObjectNode responseObj = Json.newObject();
        responseObj.put("code", 400);
        responseObj.put("message", msg);
        return ok(responseObj);
    }

    public static Result error(int code, String msg){
        ObjectNode responseObj = Json.newObject();
        responseObj.put("code", code);
        responseObj.put("message", msg);
        return ok(responseObj);
    }

    public static Result success(Object data){
        ObjectNode responseObj = Json.newObject();
        responseObj.put("code", 200);
        responseObj.put("message", "Success");
        responseObj.putPOJO("data", Json.toJson(data));
        return ok(responseObj);
    }

    public static Result success(ObjectNode dataJson){
        ObjectNode responseObj = Json.newObject();
        responseObj.put("code", 200);
        responseObj.put("message", "Success");
        responseObj.putPOJO("data", dataJson);
        return ok(responseObj);
    }

    public static Result error(Object data){
        ObjectNode responseObj = Json.newObject();
        responseObj.put("code", 400);
        responseObj.putPOJO("errors", Json.toJson(data));
        return ok(responseObj);
    }

    public static Result permissionDenied() {
        return error("Unauthorized");
    }
}
