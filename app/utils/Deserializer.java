package utils;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;

import javax.validation.ConstraintViolation;
import javax.validation.Validation;
import javax.validation.Validator;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

public class Deserializer {

    private static ObjectMapper mapper = new ObjectMapper().configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
    private static Validator validator = Validation.buildDefaultValidatorFactory().getValidator();

    public static <T> T fromJsonNode(JsonNode json, Class<T> clazz) throws JsonDataErrorException {
        return fromString(String.valueOf(json), clazz);
    }

    public static <T> T fromString(String json, Class<T> clazz) throws JsonDataErrorException {
        try {
            T t = mapper.readValue(json, clazz);
            Set<ConstraintViolation<T>> violations = validator.validate(t);
            if (violations.size() == 0) return t;
            List<String> msg = new ArrayList<>();
            for (ConstraintViolation<T> violation : violations) {
                msg.add(violation.getMessage());
            }
            throw new JsonDataErrorException(msg.toString());
        } catch (Exception e){
            throw new JsonDataErrorException(e.getMessage());
        }
    }

    public static class JsonDataErrorException extends Exception {
        public JsonDataErrorException(String msg){
            super(msg);
        }
    }
}