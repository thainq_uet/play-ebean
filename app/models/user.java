package models;

import io.ebean.Model;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.validator.constraints.br.CPF;
import play.data.validation.Constraints;

import javax.persistence.*;

@Entity
@Table(name = "user")
@Getter
@Setter

public class user extends Model {
    @Column(name = "username", nullable = false)
    private String username;
    @Column(name = "password", nullable = false)
    private String password;
}
