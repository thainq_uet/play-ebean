package controllers;

import apimodels.AddUserRQ;
import play.data.DynamicForm;
import play.data.FormFactory;
import play.mvc.Controller;
import play.mvc.Result;
import services.models.SVResult;
import services.userService;
import utils.*;

import javax.inject.Inject;

public class app extends Controller {
    private final userService userSer;

    @Inject
    public app(userService userSer) {
        this.userSer = userSer;
    }

    @Inject
    FormFactory formFactory;

    public Result test() {
        try {
            DynamicForm reqData = formFactory.form().bindFromRequest();
            AddUserRQ newUser = new AddUserRQ();
            newUser.setUsername(reqData.get("username"));
            newUser.setPassword(reqData.get("password"));
//        String username  = reqData.get("username");
            SVResult result = userSer.createNewUser(newUser);
            if (result.getCode() == SVResult.CODE.SUCCESS) {
                return SimpleResult.success();
            } else {
                return SimpleResult.error(result.getCode().code(), result.getMessage());
            }
        } catch (Exception e) {
            e.printStackTrace();
            return SimpleResult.systemError();
        }
    }

}
