package apimodels;


import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class AddUserRQ {
    private String username;
    private String password;

}
