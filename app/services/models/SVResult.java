package services.models;

import lombok.Getter;

@Getter
public class SVResult<T> {

    private CODE code;

    private String message;

    private T data;

    public enum CODE {

        NO_CONTENT(204),
        SUCCESS(200),
        BAD_REQUEST(400),
        UNAUTHORIZED(401),
        NOT_FOUND(404);

        private final int value;

        CODE(int value) {
            this.value = value;
        }

        public int getValue() {
            return value;
        }

        public int code() {
            return value;
        }
    }

    public SVResult() {
        this.code = CODE.SUCCESS;
        this.message = "success";
    }

    public SVResult<T> setCode(CODE code) {
        this.code = code;
        return this;
    }

    public SVResult<T> setMessage(String message) {
        this.message = message;
        return this;
    }

    public SVResult<T> setData(T data) {
        this.data = data;
        return this;
    }
}
