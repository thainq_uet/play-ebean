package services;

import javax.inject.Inject;
import javax.inject.Singleton;
import apimodels.AddUserRQ;
import repository.userRepos;
import services.models.SVResult;
import models.user;


@Singleton
public class userService {
    private final userRepos userRep;

    @Inject
    public userService(userRepos userRep) {
        this.userRep = userRep;
    }

    public SVResult createNewUser(AddUserRQ addUser) {
        user newUser = new user();
        newUser.setUsername(addUser.getUsername());
        newUser.setPassword(addUser.getPassword());
        userRep.create(newUser);
        return new SVResult();
    }
}
